package com.webpil.apps.security;


import com.webpil.apps.entity.webpil.User;
import com.webpil.apps.model.UserRoleAccess;
import com.webpil.apps.repo.webpil.AccessRepo;
import com.webpil.apps.repo.webpil.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers("/login","/assets/**","/images/**").permitAll()
                        .anyRequest().authenticated()
                )
                .csrf(csrf->csrf.disable())
                .httpBasic(Customizer.withDefaults())
                .formLogin(form -> form
                        .loginPage("/login")
                        .permitAll());
        return http.build();
    }

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private AccessRepo accessRepo;
    @Bean
    public UserDetailsService userDetailsService() {

        return  new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) {
                Optional<User> user = userRepo.findByUsername(username);
                if (user.isPresent() && user.get().getIsActive() && user.get().getEnableLogin() ){
                    return new org.springframework.security.core.userdetails.User(user.get().getUsername(), user.get().getPassword(),
                            getAuthorities(user.get())    );
                }else {
                    user = userRepo.findByEmail(username);
                    if (user.isPresent() && user.get().getIsActive() && user.get().getEnableLogin() ){
                        return new org.springframework.security.core.userdetails.User(user.get().getUsername(), user.get().getPassword(),
                                getAuthorities(user.get())    );
                    }else {
                        throw new UsernameNotFoundException("User not found: " + username);
                    }

                }
            }
        };
    }

    public Collection<? extends GrantedAuthority> getAuthorities(User user) {

        List<UserRoleAccess> accesses = accessRepo.findAuthority(user.getId());
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (UserRoleAccess access : accesses) {
            authorities.add(new SimpleGrantedAuthority(access.getAccessName().toUpperCase()));
        }

        return authorities;
    }

    @Bean
    public AuthenticationManager authenticationManager(
            UserDetailsService userDetailsService,
            PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        return new ProviderManager(authenticationProvider);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}