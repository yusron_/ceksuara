package com.webpil.apps.model;

public interface UserRoleAccess {

    Long getId();
    String getName();
    String getUsername();
    String getEmail();
    Long getRoleId();
    String getRoleName();
    Long getAccessId();
    String getAccessName();
}
