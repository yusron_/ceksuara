package com.webpil.apps.entity.webpil;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String username;
    private String password;
    private String email;
    private LocalDateTime co;
    private Long cb;
    private LocalDateTime uo;
    private Long ub;

    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "enable_login")
    private Boolean enableLogin;

}
