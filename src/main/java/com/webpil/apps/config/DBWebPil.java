package com.webpil.apps.config;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({ "classpath:application.properties" })
@EnableJpaRepositories(basePackages = "com.webpil.apps.repo.webpil", entityManagerFactoryRef = "WebpilEntityManager", transactionManagerRef = "WebpilTransactionManager")
public class DBWebPil {
    @Autowired
    private Environment env;

    @Bean
    public EntityManagerFactory WebpilEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(WebpilDataSource());
        em.setPackagesToScan(new String[]{"com.webpil.apps.entity.webpil"});

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        em.setJpaPropertyMap(properties);
        em.afterPropertiesSet();
        return em.getObject();
    }

    @Bean
    public DataSource WebpilDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.webpil.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.webpil.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.webpil.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.webpil.datasource.password"));

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager WebpilTransactionManager() {

        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(WebpilEntityManager());
        return transactionManager;
    }
}