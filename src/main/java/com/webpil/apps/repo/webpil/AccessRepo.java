package com.webpil.apps.repo.webpil;

import com.webpil.apps.entity.webpil.Access;
import com.webpil.apps.model.UserRoleAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccessRepo extends JpaRepository<Access,Long> {

    @Query(nativeQuery= true,value="select u.id, u.name ,u.name ,u.email ,r.id roleId,r.name roleName,a.id accessId, a.name accessName from users u \n" +
            "inner join user_role ur on ur.user_id = u.id \n" +
            "inner join roles r  on ur.role_id = r.id \n" +
            "inner join role_access ra on ra.role_id = r.id \n" +
            "inner join accesses a on ra.access_id  = a.id\n" +
            "where u.id  = :userid")
    List<UserRoleAccess> findAuthority(@Param("userid") Long userid);
}
