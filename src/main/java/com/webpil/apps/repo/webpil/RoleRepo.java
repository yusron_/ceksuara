package com.webpil.apps.repo.webpil;

import com.webpil.apps.entity.webpil.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role,Long> {
}
