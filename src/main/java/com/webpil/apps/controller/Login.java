package com.webpil.apps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class Login {
    @GetMapping("/login")
    String login() {
        return "auth/sign-in";
    }
}