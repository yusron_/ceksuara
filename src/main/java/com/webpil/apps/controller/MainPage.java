package com.webpil.apps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class MainPage {
    @GetMapping("/")
    String login() {

        return "pages/home";
    }
}